package FilaDE;


public class LDE {
    private No inicio;
    private No fim;
    int qtdListas=0;

    public void AdicionarInicio(int valor) {
        No novo = new No(valor);
        if(inicio==null ) {
            inicio = novo;
            fim = novo;
        }else {
	        novo.anterior=fim;
	        fim.proximo=novo;
	        fim=novo;
        }
        qtdListas++;

    }
//================================================================================//
    public void RemoverInicio() {
    	No posteriorInicio=inicio.proximo;
    	
    	No atual=inicio.proximo;
    	
    	atual.anterior.proximo=null;
    	atual.anterior=null;
    	inicio=posteriorInicio;
    	
    	qtdListas--;
    }
    
//================================================================================//
    
    public void BuscarValor(int valor){
    	if(encontrarValor(valor)) 
    		System.out.println("O valor "+valor+" foi encontrado na posicao: "+Posicao(valor) );
    	else 
    		System.out.println("O valor "+valor+" n�o foi encontrado!");
    }
    
    private boolean encontrarValor(int valor){
        No atual=inicio;
        
        for (int i=0; i < qtdListas; i++) {
            if(atual.dado==valor)
                return true;
            atual=atual.proximo;
        }
		return false;
    }
    
    private int Posicao(int valor){
        No atual=inicio;
        int i;
        for (i=1; i <= qtdListas; i++) {
            if(atual.dado==valor)
                break;
            atual=atual.proximo;
        }
		return i;
    }
  //--------------------------------------------------------//
    
//================================================================================//
    public void Listar() {
    	No novo= inicio;
        System.out.println("\nPrintando Lista: ");
        
        while(novo!=null) {
            System.out.print(novo.dado+", ");
            novo=novo.proximo;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
        System.out.println("Dado Final: "+fim.dado);
    }
//--------------------------------------------------------------------------//

    public void ListarContrario() {
    	No novo= fim;
    	
        System.out.println("\nPrintando Lista Ao Contrario: ");
        
        while(novo!=null) {
            System.out.print(novo.dado+", ");
            novo=novo.anterior;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
        System.out.println("Dado Final: "+inicio.dado);
    }
//--------------------------------------------------------------------------//
	
	/*public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		
		LDE other = (LDE) obj;
		
		if (qtdListas != other.qtdListas)
			return false;
		else {
			
		}
		return true;
	}*/
    
    
    


}
