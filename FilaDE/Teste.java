package FilaDE;

public class Teste {
    public static void main(String[] args) {
        LDE LDE = new LDE();

        System.out.println("            Adicionando Valores No Inicio!");
        LDE.AdicionarInicio(1);
        LDE.AdicionarInicio(2);
        LDE.AdicionarInicio(3);
        LDE.AdicionarInicio(4);
        LDE.AdicionarInicio(5);
        LDE.AdicionarInicio(6);
        LDE.AdicionarInicio(7);
        LDE.Listar();
        System.out.println("\n=========================================================\n\n");
        
//-----------------------------------------------------------------------------------------------//
        
        System.out.println("            Remover Valores!");
        LDE.RemoverInicio();
        LDE.Listar();
        System.out.println("-------------------------------------");
        LDE.RemoverInicio();
        LDE.Listar();
        
//-----------------------------------------------------------------------------------------------//
        System.out.println("\n=========================================================\n\n");
        
        System.out.println("            Buscando Valores!\n");
        LDE.BuscarValor(3);
        LDE.BuscarValor(8);
        LDE.BuscarValor(0);
        System.out.println("\n=========================================================\n\n");
        
//-----------------------------------------------------------------------------------------------//
        
        

    }


}
