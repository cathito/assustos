package FilaDEC;


public class LDEC {
    private No inicio;
    int qtdListas=0;

    public void AdicionarInicio(int valor) {
        No novo = new No(valor);
        
        System.out.println("Adicionando o Valor: "+valor);
        
        if(inicio==null ) {
            novo.anterior=novo;
            novo.proximo=novo;
            inicio=novo;
        }else {
        	novo.anterior=inicio.anterior;
            novo.proximo=inicio;
            inicio.anterior.proximo=novo;
            inicio.anterior=novo;
        }
	    qtdListas++;

    }
//--------------------------------------------------------------//
    
    public void RemoverInicio() { 
    	No posteriorInicio=inicio.proximo;
    	inicio=posteriorInicio;
    }
//--------------------------------------------------------------------------//
    
    public void BuscarValor(int valor){
    	if(encontrarValor(valor)) 
    		System.out.println("O valor "+valor+" foi encontrado na posicao: "+Posicao(valor) );
    	else 
    		System.out.println("O valor "+valor+" n�o foi encontrado!");
    }
    
    private boolean encontrarValor(int valor){
        No atual=inicio;
        
        for (int i=0; i < qtdListas; i++) {
            if(atual.dado==valor)
                return true;
            atual=atual.proximo;
        }
		return false;
    }
    
    private int Posicao(int valor){
        No atual=inicio;
        int i;
        for (i=1; i <= qtdListas; i++) {
            if(atual.dado==valor)
                break;
            atual=atual.proximo;
        }
		return i;
    }
    

//================================================================================//
    
    public void Listar() {
    	No novo= inicio;
    	int pulo=0;
        int contadorLup=0;

        System.out.println("\nPrintando Lista: ");
        
        while(true) {
        	
            if( pulo==qtdListas){
            	System.out.print(" //  ");
            	pulo=0;
            }
            if (contadorLup==3*qtdListas)
            	break;
            
            System.out.print(novo.dado+", ");
            novo=novo.proximo;
            contadorLup++;
            pulo++;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
    }
//--------------------------------------------------------------------------//

    public void ListarContrario() {
    	No novo= inicio.anterior;
    	int pulo=0;
        int contadorLup=0;

        System.out.println("\nPrintando Lista Ao Contrario: ");
        while(true) {
        	if( pulo==qtdListas){
            	System.out.print(" //  ");
            	pulo=0;
            }
            if (contadorLup==3*qtdListas)
            	break;
            
            System.out.print(novo.dado+", ");
            novo=novo.anterior;
            contadorLup++;
            pulo++;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
    }
//--------------------------------------------------------------------------//*/


}
