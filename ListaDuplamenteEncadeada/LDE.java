package ListaDuplamenteEncadeada;


public class LDE {
    private No inicio;
    private No fim;
    int qtdListas=0;

    public void AdicionarInicio(int valor) {
        No novo = new No(valor);
        if(inicio==null ) {
            inicio = novo;
            fim = novo;
        }else {
            novo.proximo=inicio;
            inicio.anterior=novo;
            inicio=novo;
        }
        qtdListas++;
    }
//--------------------------------------------------------------------------//
    
    public void AdicionarMeio(int posicao, int valor ) {
    	if(posicao<2) 
    		AdicionarInicio(valor);
    	
    	else if(posicao>this.qtdListas) 
    		AdicionarFim(valor);
    	
    	else {
	        No novo= new No(valor);
	        No atual=inicio;
	        
	        for (int i = 1; i < posicao-1; i++) {
	            atual=atual.proximo;
	        }
	        novo.proximo=atual.proximo;
	    	novo.anterior=atual;
	    	atual.proximo=novo;
	    	novo.proximo.anterior=novo;
	    	qtdListas++;
    	}
    	
    }
//--------------------------------------------------------------------------//

    public void AdicionarFim(int valor) {
    	No novo = new No(valor);
        
        novo.anterior=fim;
        fim.proximo=novo;
        fim=novo;
        qtdListas++;

    }
//================================================================================//
    public void RemoverInicio() {
    	inicio=inicio.proximo;
    	inicio.anterior=null;
    	
    	qtdListas--;
    }
  //--------------------------------------------------------//
    public void RemoverMeio(int posicao) {
    	if(posicao<2) 
    		RemoverInicio();
    	
    	else if(posicao>this.qtdListas) 
    		RemoverFim();
    	
    	else{
    		No atual=inicio;
	    	for (int i = 1; i < posicao-1; i++) {
	            atual=atual.proximo;
	        }
	    	atual.proximo.proximo.anterior=atual;
	    	atual.proximo=atual.proximo.proximo;
	    	qtdListas--;
    	}
    	
    }
  //--------------------------------------------------------//
    public void RemoverFim() {
    	No anteriorFim=fim.anterior;
    	
    	No atual=fim;
    	
    	atual.anterior.proximo=null;
    	atual.anterior=null;
    	fim=anteriorFim;
    	qtdListas--;
    }
//================================================================================//
    
    public void BuscarValor(int valor){
    	if(encontrarValor(valor)) 
    		System.out.println("O valor "+valor+" foi encontrado na posicao: "+Posicao(valor) );
    	else 
    		System.out.println("O valor "+valor+" n�o foi encontrado!");
    }
    
    private boolean encontrarValor(int valor){
        No atual=inicio;
        
        for (int i=0; i < qtdListas; i++) {
            if(atual.dado==valor)
                return true;
            atual=atual.proximo;
        }
		return false;
    }
    
    private int Posicao(int valor){
        No atual=inicio;
        int i;
        for (i=1; i <= qtdListas; i++) {
            if(atual.dado==valor)
                break;
            atual=atual.proximo;
        }
		return i;
    }
  //--------------------------------------------------------//
    
    public void trocaValorLocal(int valor,int posicao){
        if(encontrarValor(valor)){
        	BuscarValor(valor);
        	
            RemoverMeio(Posicao(valor));
            
            AdicionarMeio(posicao, valor);
        }
        
    }
//================================================================================//
    public void Listar() {
    	No novo= inicio;
        System.out.println("\nPrintando Lista: ");
        
        while(novo!=null) {
            System.out.print(novo.dado+", ");
            novo=novo.proximo;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
        System.out.println("Dado Final: "+fim.dado);
    }
//--------------------------------------------------------------------------//

    public void ListarContrario() {
    	No novo= fim;
    	
        System.out.println("\nPrintando Lista Ao Contrario: ");
        
        while(novo!=null) {
            System.out.print(novo.dado+", ");
            novo=novo.anterior;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
        System.out.println("Dado Final: "+inicio.dado);
    }
//--------------------------------------------------------------------------//


}
