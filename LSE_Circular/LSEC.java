package LSE_Circular;


public class LSEC {
	
	private No inicio;
	private No fim;
	int qtdListas=0;
	
	public void AdicionarInicio(int valor) {
		No novo = new No(valor);
		
		if(inicio==null ) {		
			novo.proximo=novo;
			fim=novo;
		}else {
			novo.proximo=inicio;
			fim.proximo=novo;
		}
		
		inicio=novo;
		qtdListas++;
	}
//----------------------------------------------------//
	public void AdicionarMeio(int posicao, int valor) {
		No novo= new No(valor);
		No atual= inicio;
		
		if(posicao<2) 
    		AdicionarInicio(valor);
    	
		else if(posicao>this.qtdListas) 
    		AdicionarFim(valor);
    	
    	else {
			for (int i = 1; i < posicao-1; i++) {
				atual=atual.proximo;
			}
			novo.proximo=atual.proximo;
			atual.proximo=novo;
			qtdListas++;
    	}
	}
//----------------------------------------------------//
	
	public void AdicionarFim(int valor) {
		No novo = new No(valor);
		fim.proximo=novo;
		novo.proximo=inicio;
		fim=novo;
		qtdListas++;
		
	}
//=============================================================================//
	public void RemoverInicio() {
		inicio=inicio.proximo;
		fim.proximo=inicio;
		qtdListas--;
		
	}
//----------------------------------------------------//
	public void RemoverMeio(int posicao) {
		
		if(posicao<2) 
    		RemoverInicio();
    	
		else if(posicao>this.qtdListas) 
    		RemoverFim();
    	else {
    		No atual=inicio;
    		for (int i = 1; i < posicao-1; i++) {
    			atual=atual.proximo;
    		}
    		
    		atual.proximo=atual.proximo.proximo;
    		qtdListas--;
    	}
    	
	}
	
	
//----------------------------------------------------//
	public void RemoverFim() {
		No atual= inicio;
		for (int i = 1; i < qtdListas-1; i++) {
			atual=atual.proximo;
		}
		atual.proximo=inicio;
		fim=atual;
		qtdListas--;
		
	}
//=============================================================================//
	
	public void Listar() {
    	No novo= inicio;
        int contadorLup=0;

        System.out.println("\nPrintando Lista: ");
        while(true) {
            if(contadorLup==qtdListas*2){
                break;
            }
            if( (qtdListas*2) /2==contadorLup){
            	System.out.print(" //  ");
            }
            
            System.out.print(novo.dado+", ");
            novo=novo.proximo;
            contadorLup++;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
    }
//----------------------------------------------------//
	
	
	
	
	
	
	

}
