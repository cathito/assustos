package FilaSE;

public class TesteLista {

	public static void main(String[] args) {
		FilaSE ls= new FilaSE();
		
		System.out.println("\nAdicionando Valores Na Fila 1.\n");
		ls.Adicionar(1);
		ls.Adicionar(2);
		ls.Adicionar(3);
		ls.Adicionar(4);
		ls.Adicionar(5);
		System.out.println("");
		ls.Listar();
		System.out.println("\n\n===============================\n");
		
		FilaSE ls2= new FilaSE();
		System.out.println("\nAdicionando Valores Na Fila 2.\n");
		ls2.Adicionar(1);
		ls2.Adicionar(2);
		ls2.Adicionar(3);
		ls2.Adicionar(4);
		ls2.Adicionar(5);
		System.out.println("");
		ls2.Listar();
		System.out.println("\n\n===============================\n");
//-------------------------------------------------------------------------------//
		
		if(ls.equals(ls2)) 
			System.out.println("Iguais!");
		else
			System.out.println("Diferentes!");
//-------------------------------------------------------------------------------//
		
		System.out.println("\n\n===============================\n");
		ls.Listar();
		System.out.println("\n\n===============================\n");
		ls2.Listar();
		System.out.println("\n\n===============================\n");
//-------------------------------------------------------------------------------//
		
		System.out.println("Removendo Valores da Fila.");
		ls.Remover();
		ls.Listar();
		System.out.println("\n--------------------------");
		ls.Remover();
		ls.Listar();
		System.out.println("\n\n===============================\n");
		


	}

}
