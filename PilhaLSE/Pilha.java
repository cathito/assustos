package PilhaLSE;

public class Pilha {
	
	private No topo;
	int qtdListas=0;
//----------------------------------------------------//
	
	public void empilhar(int valor) {
		No novo = new No(valor);

		if (topo == null){
			topo=novo;
		}else {
			novo.proximo=topo;
			topo=novo;
		}
		qtdListas++;
	}
//----------------------------------------------------//
	public void desempilhar() {
		if(topo!=null){
			topo=topo.proximo;
			qtdListas--;
		}else {
			System.out.println("A Pilha esta Vazia!");
		}

	}
//====================================================================//
	public void encontrarValor(int valor) {
		Pilha lsAuxiliar=this;
		
		No novo= lsAuxiliar.topo;
		int elementoNumero=0;
		
		while(novo!=null) {
			elementoNumero++;
			
			if(novo.dado==valor) {
				System.out.println(
						    "               Valor Encontrado! "
						+ "\nValor correspodente ao "+elementoNumero+"� elemento a ser Desempilhado!");
				break;
			}
			desempilhar();
			novo=novo.proximo;
		}
		
	}
	
//====================================================================//
	public void Listar() {
		No novo= topo;
		while(novo!=null) {
			System.out.println("| "+novo.dado+" |");
			novo=novo.proximo;
		}
		System.out.println("Tamanho da Pilha � : "+qtdListas);
	}
//====================================================================//
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		
		Pilha other = (Pilha) obj;
		Pilha origin=this;
		
		if (qtdListas != other.qtdListas)
			return false;
		else {
			for (int i = 0; i < this.qtdListas; i++) {
				if(origin.topo.dado!=other.topo.dado)
					return false;
				origin.desempilhar();
				other.desempilhar();
			}
		}
		return true;
	}
	
	
	
	
	
	
	

}
