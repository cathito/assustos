package PilhaLSE;

public class TesteLista {//empilhar, desempilhar,topo

	public static void main(String[] args) {
		
		Pilha ls= new Pilha();
		System.out.println("\nAdicionando Valores Na Pilha 1.");
		
		ls.empilhar(1);
		ls.empilhar(2);
		ls.empilhar(3);
		ls.empilhar(4);
		ls.empilhar(5);
//-----------------------------------------------------------------------//
		Pilha ls2= new Pilha();
		System.out.println("\nAdicionando Valores Na Pilha 2.\n");
		
		ls2.empilhar(1);
		ls2.empilhar(2);
		ls2.empilhar(3);
		ls2.empilhar(4);
		ls2.empilhar(5);
		
		System.out.println("----------------------------------");
//-----------------------------------------------------------------------//
		System.out.println("        Pilha 1");
		ls.Listar();
		System.out.println("-----------------------\n");
		System.out.println("        Pilha 2");
		ls2.Listar();
		System.out.println("===============================\n");
		
		ls.desempilhar();
		System.out.println("        Pilha 1");
		ls.Listar();
		System.out.println("-----------------------\n");
		

	}

}
